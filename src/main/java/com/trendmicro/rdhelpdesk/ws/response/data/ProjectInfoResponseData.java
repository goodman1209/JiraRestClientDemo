package com.trendmicro.rdhelpdesk.ws.response.data;

import java.io.Serializable;

public class ProjectInfoResponseData extends BaseResponseData implements Serializable {

	/** */
	private static final long serialVersionUID = 1L;

	private String projectNameFull;

	private String projectNameAbbreviation;

	private String version;

	public String getProjectNameFull() {
		return projectNameFull;
	}

	public void setProjectNameFull(String projectNameFull) {
		this.projectNameFull = projectNameFull;
	}

	public String getProjectNameAbbreviation() {
		return projectNameAbbreviation;
	}

	public void setProjectNameAbbreviation(String projectNameAbbreviation) {
		this.projectNameAbbreviation = projectNameAbbreviation;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
