package com.trendmicro.rdhelpdesk.ws.response.data;

import java.io.Serializable;

public class BaseResponseData implements Serializable {

	/** */
	private static final long serialVersionUID = 1L;

	private boolean error;

	private String message;

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
