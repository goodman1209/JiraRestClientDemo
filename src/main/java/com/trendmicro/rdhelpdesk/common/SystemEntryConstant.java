package com.trendmicro.rdhelpdesk.common;

public class SystemEntryConstant {

	public static final String CODE_SIGN_APPROVE = "0";

	public static final String CODE_SIGN_TRIGGER = "1";
}
