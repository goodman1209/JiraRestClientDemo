package com.trendmicro.rdhelpdesk.command;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.domain.Issue;

public class IssueService extends SystemObject {
	public IssueService(JiraRestClient restClient) {
		super(restClient);
	}

	public Issue findIssue(String issueKey) {
		return restClient.getIssueClient().getIssue(issueKey).claim();
	}
}
