package com.trendmicro.rdhelpdesk.command;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.domain.Comment;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.input.FieldInput;
import com.atlassian.jira.rest.client.domain.input.TransitionInput;
import com.google.common.util.concurrent.FutureCallback;

public class ActionService extends SystemObject {
	private static final Logger LOG = LoggerFactory.getLogger(ActionService.class);

	public ActionService(JiraRestClient restClient) {
		super(restClient);
	}

	public void transitionByIssue(Issue issue, int action, String message) {
		// final FieldInput fieldInput = new
		// FieldInput(actual.getFieldByName("test").getId(), "WTF1");
		// final Collection<FieldInput> fieldInputs = Arrays.asList(fieldInput);

		// add comment in transition is not work!!
		restClient.getIssueClient().transition(issue, new TransitionInput(action, Comment.valueOf(message)))
		        .then(getCallBackInstamce()).claim();
	}

	public void transitionByIssue(Issue issue, int action, Collection<FieldInput> fieldInputs, String message) {
		restClient.getIssueClient()
		        .transition(issue, new TransitionInput(action, fieldInputs, Comment.valueOf(message)))
		        .then(getCallBackInstamce()).claim();
	}

	public FutureCallback<Void> getCallBackInstamce() {
		return new FutureCallback<Void>() {

			public void onSuccess(Void result) {
				exitSystem("Allan result Success");
			}

			public void onFailure(Throwable t) {
				exitSystem("Allan result Error");
			}
		};
	}

	public void exitSystem(String message) {
		LOG.info("message : {}", message);
		System.exit(0);
	}
}
