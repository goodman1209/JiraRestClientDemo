package com.trendmicro.rdhelpdesk.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.domain.Issue;
import com.trendmicro.rdhelpdesk.common.ActionConstant;

public class CodeSignService {
	private static final Logger LOG = LoggerFactory.getLogger(CodeSignService.class);
	private IssueService is;
	private ActionService as;

	public CodeSignService(JiraRestClient restClient) {
		this.is = new IssueService(restClient);
		this.as = new ActionService(restClient);
	}

	public void approveCodeSignByManager(String issueKey) {
		LOG.info("CodeSign approveCodeSignByManager : {} sign start", issueKey);
		Issue issue = is.findIssue(issueKey);
		// final FieldInput fieldInput = new
		// FieldInput(issue.getFieldByName("assignee").getId(), "s-codesign");
		// final Collection<FieldInput> fieldInputs = Arrays.asList(fieldInput);
		as.transitionByIssue(issue, ActionConstant.REQUEST_CODE_SIGN, "System approveCodeSignByManager");
		LOG.info("CodeSign approveCodeSignByManager : {} sign end", issueKey);
	}

	public void sign(String issueKey) {
		LOG.info("CodeSign : {} sign start", issueKey);
		Issue issue = is.findIssue(issueKey);
		as.transitionByIssue(issue, ActionConstant.DONE, "System transition");
		LOG.info("CodeSign : {} sign end", issueKey);
	}
}
