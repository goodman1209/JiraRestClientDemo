package com.trendmicro.main;

import java.net.URISyntaxException;
import java.util.Properties;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.domain.Field;
import com.google.common.util.concurrent.FutureCallback;
import com.trendmicro.rdhelpdesk.command.CodeSignService;
import com.trendmicro.rdhelpdesk.command.IssueService;
import com.trendmicro.rdhelpdesk.command.utils.ConnectJiraUtils;
import com.trendmicro.rdhelpdesk.common.SystemEntryConstant;

public class Main {
	private static final Logger LOG = LoggerFactory.getLogger(Main.class);

	public static void main(String args[]) throws URISyntaxException, JSONException {

		JiraRestClient restClient = makeRestClientObject();
		LOG.info("restClient.getIssueClient() {} key : {}", restClient, args[0]);

		String event = args[0];
		String issueKey = args[1];
		if (SystemEntryConstant.CODE_SIGN_APPROVE.equals(event)) {
			CodeSignService css = new CodeSignService(restClient);
			css.approveCodeSignByManager(issueKey);
		} else if (SystemEntryConstant.CODE_SIGN_TRIGGER.equals(event)) {
			LOG.info("{} transition if error ", issueKey);
			// CodeSignService css = new CodeSignService(restClient);
			// css.sign(issueKey);
		} else {
			LOG.info("{} test function ", issueKey);
			IssueService is = new IssueService(restClient);
			LOG.info("{} ", jsonTransitionIssue(null, null));
			ConnectJiraUtils.update(is.findIssue(issueKey), jsonTransitionIssue(null, null), true, "s-codesign",
			        "jirajira");
		}
	}

	public static JiraRestClient makeRestClientObject() {
		try {
			Properties properties = new Properties();
			properties.load(Main.class.getClassLoader().getResourceAsStream("system.properties"));
			return ConnectJiraUtils.GetJiraRestClientInstance(properties.getProperty("username"),
			        properties.getProperty("password"));
		} catch (Exception e) {
			LOG.error("restClient Error {}", e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	private static JSONObject jsonEditIssue(Field field, String value) throws JSONException {
		// "timetracking":{"remainingEstimate":"0m","timeSpent":"3w","remainingEstimateSeconds":0,"timeSpentSeconds":432000}
		JSONObject summary = new JSONObject().accumulate(field.getId(), value);
		JSONObject fields = new JSONObject().accumulate("fields", summary);
		return fields;
	}

	private static JSONObject jsonTransitionIssue(Field field, String value) throws JSONException {

		JSONObject commentBody = new JSONObject().accumulate("body", "system added");
		JSONObject commentAdd = new JSONObject().accumulate("add", commentBody);
		JSONArray comments = new JSONArray();
		comments.put(commentAdd);
		JSONObject comment = new JSONObject().put("comment", comments);
		JSONObject transitionAction = new JSONObject().accumulate("id", "31");
		JSONObject fields = new JSONObject().put("update", comment);
		fields = fields.accumulate("transition", transitionAction);

		JSONObject assignName = new JSONObject().accumulate("name", "s-codesign");
		JSONObject assignee = new JSONObject().accumulate("assignee", assignName);
		// JSONObject resoName = new JSONObject().accumulate("name",
		// "Unresolved");
		// assignee = assignee.accumulate("resolution", resoName);
		// fields = fields.accumulate("fields", assignee);
		return fields;
	}

	public static FutureCallback<Void> getCallBackInstamce() {
		return new FutureCallback<Void>() {

			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				LOG.info("Allan result Success");
				System.exit(0);
			}

			public void onFailure(Throwable t) {
				// TODO Auto-generated method stub
				LOG.info("Allan result Error");
				System.exit(0);
			}
		};
	}
}
